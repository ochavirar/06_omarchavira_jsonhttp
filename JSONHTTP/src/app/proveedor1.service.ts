import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Proveedor1Service {

  constructor(private http: HttpClient) { 
    console.log('Hello proveedor1 provider');
  }

  obtenerDatos(){
    return this.http.get('https://jsonplaceholder.typicode.com/users');
    console.log('I got the page!')
  }
}
