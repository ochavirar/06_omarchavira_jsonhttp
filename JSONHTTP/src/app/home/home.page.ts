import { Component } from '@angular/core';
import { Proveedor1Service } from '../proveedor1.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  usuarios: any;
  constructor(private proveedor: Proveedor1Service) {}

  ngOnInit(){
    this.proveedor.obtenerDatos().subscribe(data => {this.usuarios = data;
    console.log(data);
    });
  }
}
